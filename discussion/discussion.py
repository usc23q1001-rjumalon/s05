#Encapsulation
class Person():

	def __init__(self):
		self._name = "John Doe"
		self._age = 30

	def get_name(self):
		print(f'Name of Person:{self._name}')

	def set_age(self, age):
		self._name = name

	def get_age(self):
		print(f'Age of Person:{self._age}')

	def set_age(self, age):
		self._age = age

# tao = Person()

# tao.get_age()
# tao.set_age(40)
# tao.get_age()

#Inheritance

# class Employee(Person):

# 	def __init__(self, employeeId):
# 		super().__init__()
# 		self._employeeId = employeeId

# 	def get_employeeId(self):
# 		printf(f'The Employee ID is {self._employeeId}')

# 	def set_employeeId(self, employeeId):
# 		self._employeeId = employeeId

# 	def get_details(self):
# 		print(f"{self._employeeId} belongs to {self._name}")

# emp1 = Employee("Emp-001")
# emp1.get_details()


class Student(Person):
	def __init__(self, stundentNumber, course, yearLevel):
		super().__init__()
		self._studentNumber = stundentNumber
		self._course = course
		self._yearLevel= yearLevel

	def get_studentNumber(self):
		print(f'Student Number of Person:{self._studentNumber}')

	def set_studentNumber(self, stundentNumber):
		self._studentNumber = stundentNumber

	def get_course(self):
		print(f'Course of Person:{self._course}')

	def set_course(self, course):
		self._course = course

	def get_yearLevel(self):
		print(f'Year level of Person:{self._yearLevel}')

	def set_yearLevel(self, yearLevel):
		self._yearLevel = yearLevel

	def get_details(self):
		print(f"{self._name} is currently in year {self._yearLevel} taking up {self._course}")

stud = Student("stdt-001", "BSCS" , 4)
stud.get_details()




